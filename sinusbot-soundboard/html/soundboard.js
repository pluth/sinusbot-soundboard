var instanceId;
var files;
var playlists;

var instancesPromise = performRequest('GET', '/instances/');
var filesPromise = performRequest('GET', '/files/');
var playlistsPromise = performRequest('GET', '/playlists/');

$(document).ready(function(){
    
    var $template = $('#playlist-template');
    
    $.when(instancesPromise, filesPromise, playlistsPromise).done(function(i, f, p) {
        instanceId = i[0][0].uuid;
        files = f[0];
        playlists = p[0];
        
        playlists.forEach(function(playlist) {
            performRequest('GET', '/playlists/' + playlist.uuid).done(function(data) {
                $clone = $($template.html());
                $clone.find('.playlist-headline').html(data.name);
                data.entries.forEach(function(entry) {
                    $clone.find('.container').append($('<input />').attr({
                        type : 'button', 
                        name: entry.file, 
                        value: getFileById(entry.file).title
                    }).on('click', function() {
                            performRequest('POST', '/i/' + instanceId + '/play/byId/' + entry.file);
                    }));
                });
                
                $('body').append($clone);
            });           
        });
    });
});

function performRequest(method, apipath) {
    return $.ajax({
        method: method,
        url: '/api/v1/bot' + apipath,
        headers: {
                'Authorization': 'bearer ' + window.localStorage.token
        }
    })
}

function getFileById(id) {
    /*var toReturn;
    files.some(function(file) {
        if (file.uuid == id) {
            toReturn = file;
            return true;
        }
    });
    return toReturn; */
    
    return files.find(file => {
        return file.uuid == id;
    });
}