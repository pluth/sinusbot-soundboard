registerPlugin({
    name: 'sinusbot-soundboard',
    version: '0.1',
    description: 'webinterface',
    author: 'pluth',
    vars: {},
    enableWeb: true
}, function(sinusbot, config) {
    var ws = require('ws');
    var engine = require('engine');
    var event = require('event');

    event.on('ws.connect', function(id) {
        engine.log('new websocket connection; id ' + id);
    });
    event.on('ws.disconnect', function(id) {
        engine.log('websocket connection disconnected; id ' + id);
    });
    event.on('ws.data', function(id, type, data) {
        engine.log('ws.data: id ' + id + '; data: ' + data.toString());
        ws.write(id, type, data.toString());
    });
});
